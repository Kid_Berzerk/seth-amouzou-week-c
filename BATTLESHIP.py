board=[["_","_","_","_","_","_","_","_","_","_" ], 
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ]]

board2=[["_","_","_","_","_","_","_","_","_","_" ], 
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ],
       ["_","_","_","_","_","_","_","_","_","_" ]]
hitboard1=[["_","_","_","_","_","_","_","_","_","_" ], 
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ]]
hitboard2=[["_","_","_","_","_","_","_","_","_","_" ], 
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ],
           ["_","_","_","_","_","_","_","_","_","_" ]]
def printboard(board):
    for c in range(0,10):
        for x in range(0,10):
            print("|", end="")
            print(board[c][x], end="")
        print("|")
        
def place(board, row, column, rotation, size):
    if rotation=="horizontally" or rotation=="h" :         
        for c in range(column,column+size):
            board [row][c]=">"
    if rotation=="vertically" or rotation=="v":
        for c in range(row,row+size):
            board [c][column]="^"
    return(board)

def ask(board, size, ship):
        move=0
        while move==0:
            piece=int(input("what row would you like to put your "+str(ship)+" ship"))-1
            piece2=int(input("what column would you like to put your "+str(ship)+" ship"))-1
            piece3=input("how would you like to rotate your ship, horizontally or vertically")
            clearance=0
            if piece3=="horizontally" or piece3=="h" :
                if piece2+size>9:
                    clearance=1
                else: 
                    for c in range(piece2,piece2+size):
                        if board[piece][c]!="_":
                            clearance=clearance+1

            if piece3=="vertically" or piece3=="v":
                if piece+size>9:
                    clearance=1 
                else:
                    for c in range(piece,piece+size):
                        if board[c][piece2]!="_":
                            clearance=clearance+1     
            if(board[piece][piece2]=="_" and clearance==0):
                board=place(board, piece, piece2, piece3, size)
                printboard(board)
                move=1
            else:
                print("STOP! You have gone too far!")

def docking(board):
    ask(board, 2, "2x1")
    ask(board, 3, "first 3x1")
    ask(board, 3, "second 3x1")
    ask(board, 4, "4x1")
    ask(board, 5, "5x1")
    
def attack(board, hitboard):
    targetrow=int(input("what row would you like to attempt to kill"))-1
    targetcolumn=int(input("what column would you like to attempt to kill"))-1
    if(board[targetrow][targetcolumn]!="_"):
        hitboard[targetrow][targetcolumn]="x"
        print("you have struck down a part of a ship")
    else:
        hitboard[targetrow][targetcolumn]="0"
        print("you have attempted murder and failed")
    return(hitboard)
def winningchild(board, player):
    counter=0
    for z in range(0,10):
        for w in range(0,10):
            if board[w][z]!="_":
                counter=counter+1
    if counter==17:
        return (player)
    else:
        return("no one")
print("player 1")
printboard(board)
docking(board)
print("player 2")
printboard(board2)
docking(board2)
winner="no one"
while winner== "no one":
    print()
    print()
    print()
    print()
    print("Player one attack")
    printboard(hitboard1)
    hitboard1=attack(board2, hitboard1)
    printboard(hitboard1)
    winner=winningchild(hitboard1, "player 1")
    input("press enter to continue")
    if(winner=="no one"):
        print()
        print()
        print()
        print()
        print("Player two attack")
        printboard(hitboard2)
        hitboard2=attack(board, hitboard2)
        printboard(hitboard2)
        winner=winningchild(hitboard2, "player 2")
        input("press enter to continue")
if winner=="player 1":
    print("player 1 has murdered your team")
if winner=="player 2":
    print("player 2 has struck down your team")