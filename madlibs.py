'''
Created on Jul 9, 2018

@author: SummerTech
'''

color=input("enter a color: ")
superlative=input("enter a superlative: ")
adjective=input("enter an adjective: ")
bodyPartPlural=input("enter a body part: ")
bodyPart=input("enter a body part: ")
noun=input("enter a noun: ")
animalPlural=input("enter an animal: ")
adjective2=input("enter an adjective: ")
adjective3=input("enter an adjective: ")
adjective4=input("enter an adjective: ")
print("The "+color+" Dragon is the "+superlative+" Dragon of all.")
print("It has "+adjective+" "+bodyPartPlural+", and a "+bodyPart+" shaped like a "+noun+".")
print("It loves to eat "+animalPlural+" although it will eat nearly anything.")
print("It is "+adjective2+" and you must be "+adjective4+" around it or you may end up as it's meal.")