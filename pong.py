import msvcrt
import os
import time
def gross():
    kb=msvcrt.kbhit()
    if kb:
        ret=msvcrt.getch()
    else:
        ret=False
    return(ret)
player1=5
player2=5
ball=[8,7,-1,0]
board=[]
row=[]
x=0
for x in range (0,15):
    row.append(" ")
for x in range (0,15):
    board.append(row.copy())
    
def moveplayer1(player):
    for g in range (0,15):
        for b in range(0,15):
            if b==0 and g<player+5 and g>=player:
                board[g][b]="|"
            elif b==0:
                board[g][b]=" "
    return(board)
        
def moveplayer2(player):
    for g in range (0,15):
        for b in range(0,15):
            if b==14 and g<player+5 and g>=player:
                board[g][b]="|"
            elif b==14:
                board[g][b]=" "
    return(board)
    
def printboard(board):
    os.system("cls")
    print("------------------------------")
    for g in range (0,15):
        for b in range(0,15):
            print(board[g][b], end="")
            print(" ", end="")
        print()
    print("------------------------------")    
def moveball(ball):
    if ball[1]==0 or ball[1]==14:
        ball[3]=-ball[3]
    board[ball[1]][ball[0]]=" "
    ball[0]=ball[0]+ball[2]
    ball[1]=ball[1]+ball[3]
    board[ball[1]][ball[0]]="●"
    
    return(ball)
    
def paddle(ball, player, pos):
    if ball[0]==pos and ball[1]==player+2:
        ball[2]=-ball[2]

    elif ball[0]==pos and ball[1]==player+1:
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=-1
        else:
            ball[3]=-ball[3]
    elif ball[0]==pos and ball[1]==player+3:    
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=1
        else:
            ball[3]=-ball[3]
    elif ball[0]==pos and ball[1]==player+4:
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=-1
    elif ball[0]==pos and ball[1]==player:
        ball[2]=-ball[2]
        if ball[3]==0:
            ball[3]=-1
    return(ball)    
def win(ball, player, pos):
    if ball[0]<1:
        return 0
    if ball[0]==pos:

        return pos
    else:
        return"no one"
    
winner="no one"
while winner=="no one":
    move=gross()
    if move!=False and player1>0 and move.decode("utf-8")=="w":
        player1=player1-1
    if move!=False and player1<10 and move.decode("utf-8")=="s":
        player1=player1+1
    if move!=False and player2>0 and move.decode("utf-8")=="8":
        player2=player2-1
    if move!=False and player2<10 and move.decode("utf-8")=="5":
        player2=player2+1  
    x=x+1
    speed=3/x
    printboard(board)
    ball=moveball(ball) 
    winner=win(ball, player1,0)      
    ball=paddle(ball, player1,1)
    winner=win(ball, player1,0)
    ball=paddle(ball, player2,13)
    winner=win(ball, player1,0)
    winner=win(ball, player2,14)
    board=moveplayer1(player1)
    board=moveplayer2(player2)
    printboard(board)
    time.sleep(speed)
    
if winner==0:
    print("PLAYER 2 WINS!!!!!")
if winner==14:
    print("PLAYER 1 WINS!!!!")        